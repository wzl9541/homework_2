# Notes on learning the code

- Difference between "dot notation" and "bracket notation"   
    [See here](https://www.dataschool.io/pandas-dot-notation-vs-brackets/)   

- Fill missing values by last available data   
    DataFrame.fillna(method='pad')   

- 

