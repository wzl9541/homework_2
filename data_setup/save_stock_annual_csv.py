import pandas as pd
from data_setup.stock_annual import main as stock_annual
from data_setup.stock_monthly import main as stock_monthly

adata = stock_annual()
mdata = stock_monthly()
adata.to_csv(r"C:\Users\zhuoli\Dropbox\Zhuoli\Columbia B-School\2020 Spring\Big data\Homework\Homework_2\raw\stock_annual.csv")
mdata.to_csv(r"C:\Users\zhuoli\Dropbox\Zhuoli\Columbia B-School\2020 Spring\Big data\Homework\Homework_2\raw\stock_monthly.csv")
