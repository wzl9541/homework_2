#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Date: 2019-02
Code:
    Import CRSP Stock File tables (SF). Merge with Stock Events (SE) for static characteristics.

------

Dependence:
utils/check_primary_key_integrity

------

Cheat Sheet for WRDS query submission:
--------------------------------------
db.describe_table(library='compm', table='aco_pnfnda')
db.get_table()
db.list_tables(library='compm')
db.engine()
db.insp()
db.raw_sql()
db.get_row_count()
db.list_libraries()
db.schema_perm()

"""

#%% Import
import wrds
import datetime
import warnings
import pandas as pd
import numpy as np
import time
from time import strptime, strftime
from utils.pk_integrity import *


# %% Function Definition

def compd_aco_pnfnd(varlist, start_date, end_date, freq, gvkey_list=None, db=None):
    """
    Query Compustat Pension table (annual or quarterly).

    Primary key is gvkey-datadate.

    Note: WRDS web query merges pension tables to other tables that contain extra company
    information (address, website etc, 39 new columns in total).
    This module does not support those variables.

    Parameters
    ----------
    varlist: list
        List of Compustat variables to be queried. See Compustat documentation for available variables.
    start_date: string
        Sample start date (fiscal period end date >= start_date, e.g. '1930-01-01').
    end_date: string
        Sample end date (fiscal period end date <= end_date, e.g. '2017-12-01').
    freq: string
        Data frequency. Must be 'quarterly' or 'annual'. First option queries Compustat's Pension Quarterly table.
        Second one queries Compustat's Pension Annual table.
    gvkey_list: list, optional
        List of gvkeys to be included in the output table. If None, include all.
    db: WRDS connection

    Examples
    --------
    These replications (without gvkey_list) should return the same number of rows as WRDS's web query for
    Compustat daily update quarterly and annual pension files with no date or gvkey restrictions.

    df = download_compustat_aco_pnfnd(['prba'], '1973-06-01', '2050-12-31', 'annual')
    df = download_compustat_aco_pnfnd(['pprpaq'], '1973-06-01', '2050-12-31', 'quarterly', gvkey_list=['001000', '315318'])

    Note that the number of observations in quarterly table will be a bit smaller due to dropped
    gvkey-datadate duplicates. Output + dropped rows (given in UserWarning) should add up to rows
    in web query.
    """
    start_time = time.time()
    if db is None:
        db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

    assert freq in ['quarterly', 'annual'], "freq must be either 'quarterly' or 'annual'."

    aco_pnfnd_table = 'aco_pnfnda' if freq == 'annual' else 'aco_pnfndq'

    varlist = ", ".join(varlist)
    gvkeys = 'AND gvkey IN ({})'.format(', '.join("'" + item + "'" for item in gvkey_list)) if gvkey_list else ''

    sql = '''
          SELECT gvkey, datadate, {}
          FROM comp.{}
          WHERE indfmt = 'INDL'
          AND datafmt = 'STD'
          AND popsrc = 'D'
          AND consol = 'C'
          {}
          AND datadate >= DATE '{}'
          AND datadate <= DATE '{}'
          '''.format(varlist, aco_pnfnd_table, gvkeys, start_date, end_date)

    df = db.raw_sql(sql, date_cols=['datadate'])

    # In aco_pnfndq, there are 268 duplicate observations. In 235 cases, only fyr differs so it does not really
    # matter which observation to choose, so we pick largest fyr (typically 12 = December). Out of 35 cases where
    # there are differences also in other values than fyr, only in one case fyr=12 is not one of the two observations
    # (2, 5 instead). Updated 6/14/2018.
    if freq == 'quarterly':
        if 'fyr' in df.columns:
            df.sort_values(['fyr'], inplace=True)
            df = df[~df.duplicated(subset=['gvkey', 'datadate'], keep='last')].reset_index(drop=True)
        else:
            n = df.shape[0]
            df = df[~df.duplicated(subset=['gvkey', 'datadate'], keep='last')].reset_index(drop=True)
            if n - df.shape[0] is not 0:
                warnings.warn('''{} duplicate gvkey-datadate observations in aco_pnfndq were deleted arbitrarily.
                              In addition to column fyr (fiscal year-end), this only affects 35 observations
                              in the whole table (as of 6/14/2018). To get consistent results across queries, please
                              include fyr to varlist in which case largest value of fyr (12 in all but one case)
                              is chosen in conflicting cases.'''.format(n - df.shape[0]))

    pk_integrity(df, ['gvkey', 'datadate'])

    print("Pension data was successfully downloaded in {} seconds.".format(str(time.time() - start_time)))

    return df