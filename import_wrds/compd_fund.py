#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2018-06
Code:
    Import data from WRDS using wrsd package.

Notes:
------
check_primary_key_integrity
download_compustat_fund
download_aco_pnfnd
download_crsp_sf
merge_compustat_crsp

Cheat Sheet for WRDS query submission:
--------------------------------------
db.describe_table(library='compm', table='aco_pnfnda')
db.get_table()
db.list_tables(library='compm')
db.engine()
db.insp()
db.raw_sql()
db.get_row_count()
db.list_libraries()
db.schema_perm()
"""

# %% Import
import wrds
import datetime
import warnings
import pandas as pd
import numpy as np
import time
from time import strptime, strftime
from utils.pk_integrity import *


# %% Function Definition
def compd_fund(varlist, start_date, end_date, freq, gvkey_list=None, main_exchanges_only=False, db=None):
    """
    Query Compustat Fundamentals table (annual or quarterly).

    Primary key is gvkey-datadate.

    Parameters
    ----------
    varlist: list
        List of Compustat variables to be queried. See Compustat documentation for available variables.
    start_date: string
        Sample start date (fiscal period end date >= start_date, e.g. '1/31/1930').
    end_date: string
        Sample end date (fiscal period end date <= end_date, e.g. '12/31/2017').
    freq: string
        Data frequency. Must be 'quarterly' or 'annual'. First option queries Compustat's Fundamentals Quarterly table.
        Second one queries Compustat's Fundamentals Annual table.
    gvkey_list: list, optional
        List of gvkeys to be included in the output table. If None, include all.
    main_exchanges_only: boolean, default False
        If True, include only obsevations in NYSE, NASDAQ and AMEX (exchange code 11, 12 or 14).
    db: WRDS connection

    Examples
    --------
    df = download_compustat_fund(['saley', 'saleq'], '1960-01-01', '2017-12-31', 'quarterly')

    df = download_compustat_fund(['sale'], '1960-01-01', '2017-12-31', 'annual', gvkey_list=['001000', '315318'])
    """
    start_time = time.time()

    if db is None:
        db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

    assert freq in ['quarterly', 'annual'], "freq must be either 'quarterly' or 'annual'."

    fund_table = 'funda' if freq == 'annual' else 'fundq'
    calendar_view = 'AND datacqtr IS NOT NULL' if freq == 'quarterly' else ''
    exchanges = 'AND exchg IN (11, 12, 14)' if main_exchanges_only else ''
    gvkeys = 'AND gvkey IN ({})'.format(', '.join("'" + item + "'" for item in gvkey_list)) if gvkey_list else ''
    varlist = ", ".join(varlist)

    sql = '''
          SELECT gvkey, datadate, {}
          FROM comp.{}
          WHERE indfmt = 'INDL'
          AND datafmt = 'STD'
          AND popsrc = 'D'
          AND consol = 'C'
          {}
          {}
          {}
          AND datadate >= DATE '{}'
          AND datadate <= DATE '{}'
          '''.format(varlist, fund_table, calendar_view, exchanges, gvkeys, start_date, end_date)

    df = db.raw_sql(sql, date_cols=['datadate'])

    pk_integrity(df, ['gvkey', 'datadate'])

    print("Compustat data was successfully downloaded in {} seconds.".format(str(time.time() - start_time)))

    return df