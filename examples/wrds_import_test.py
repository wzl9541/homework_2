#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
Code:
    Test code.
    It imports compustat annual fundamentals, pension data, crsp monthly and crsp daily.
    It also merge compustat data with crsp.

"""

# %% Packages
import numpy as np
import pandas as pd
import wrds

from import_wrds.crsp_sf import *
from import_wrds.compd_aco_pnfnd import *
from import_wrds.compd_fund import *
from import_wrds.merge_comp_crsp import *


# %% Set Up
db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

# %% Download firms fundamentals from Compustat.
varlist = ['cusip', 'conm', 'fyear', 'fyr', 'fdate', 'apdedate', 'at']

start_date = '2017-01-01'
end_date = datetime.date.today().strftime("%Y-%m-%d")
freq = 'annual'
gvkey_list = None  # ['6066', '12141']
main_exchanges_only = False
comp_data = compd_fund(varlist=varlist, start_date=start_date, end_date=end_date, freq='annual', db=db)
comp_data.shape

# Download pension data
varlist = ['prba']
pension_data = compd_aco_pnfnd(varlist=varlist, start_date=start_date, end_date=end_date, freq=freq, db=db)

# Download CRSP data
varlist = ['cfacpr', 'cfacshr', 'cusip', 'distcd', 'divamt', 'dlret', 'dlretx', 'exchcd', 'ret']

start_date = '2015-01-01'  # '1925-01-01' #
end_date = datetime.date.today().strftime("%Y-%m-%d")
freq = 'monthly'  # 'daily'
permno_list = None  # [14593, 10107] #
shrcd_list = None  # [10, 11]  #
exchcd_list = None  # [1, 2, 3]  #
crspm = crsp_sf(varlist,
                start_date,
                end_date,
                freq=freq,
                permno_list=permno_list,
                shrcd_list=shrcd_list,
                exchcd_list=exchcd_list,
                db=db)

# Download CRSP data (daily)
varlist = ['dlret', 'dlretx', 'exchcd', 'permco', 'prc', 'ret', 'retx', 'shrcd', 'shrout']

start_date = '2017-12-01'  # '1925-01-01'  #
end_date = datetime.date.today().strftime("%Y-%m-%d")
freq = 'daily'  # 'monthly'  #
permno_list = [14593, 10107]  # None #
shrcd_list = [10, 11]  # None  #
exchcd_list = [1, 2, 3]  # None  #
crspd = crsp_sf(varlist,
                start_date,
                end_date,
                freq=freq,
                permno_list=permno_list,
                shrcd_list=shrcd_list,
                exchcd_list=exchcd_list)

# Merge CRSP and Compustat
lcomp = merge_compustat_crsp(comp_data)
lcomp.permno.unique().shape
lcomp.shape

