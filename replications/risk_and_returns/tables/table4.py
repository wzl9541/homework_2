#!/usr/bin/env /shared/share_dff/anaconda3/bin/python
# coding: utf-8

""""
# * Program          : table4.py
# * Function         : create Table 4 DMRS, 2020
# * Author           : Kent Daniel, Lira Mota, Simon Rottke and Tano Santos
# * Date             : 2020-01
# * Description      : 
#
# ## Detailed Description:
# Given the DMRS test portfolio allocation, calculates portfolio returns.
# All returns saved are EXCESS returns.
#
"""

# %% Packages
import pandas as pd
import numpy as np
import sqlalchemy as sa

import user_setup as uset
from utils.get_regresults import get_reg_results
import replications.risk_and_returns.hedge_portfolio_returns as hport_returns

desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 10)

def main():
    print('Calculate test portfolio returns have started.')
    # %% Parameters
    # User's parameters
    # -----------------
    user = 'Lira'
    mysql_vars = uset.user_setup(user)
    
    # Year range
    # ----------
    startdate = '1963-07-01'
    enddate = '2019-06-30'
    
    # Covariance 
    # ----------
    cov_type = 'nonrobust'  # 'HC0' #

    # %% Set MySQL Connection
    conn = sa.create_engine("mysql+pymysql://"+mysql_vars['SQL_USER'] +
                            ":" + mysql_vars['SQL_PASSWORD'] +
                            "@" + mysql_vars['SQL_HOST'] +
                            ":" + mysql_vars['SQL_PORT'] + "/dmrs")

    # %% Download Data
    # Monthly factors
    mfactors = pd.read_sql_query("""SELECT date, 
                                    MktRF, HML, RMW, CMA, SMB, RF
                                    FROM factors.ff5_monthly
                                    WHERE date BETWEEN '%s' AND '%s';""" % (startdate, enddate),
                                    conn, parse_dates=['date'])
    mfactors.set_index('date', inplace=True)
    
    # Long-short hedge portfolios
    hport = hport_returns.main(user=user)
    hport['EW5'] = hport.mean(axis=1)
    hport['EW4'] = hport[['b', 'h', 'r', 'c']].mean(axis=1)
    hport['EW3'] = hport[['h', 'r', 'c']].mean(axis=1)
 
    # %% Table: Regressions on hedge portfolios
    perioddata = hport.join(mfactors, how='inner') * 100

    overview_table = pd.DataFrame()
    for load in ['b', 's', 'h', 'r', 'c', 'EW3', 'EW4', 'EW5']:
        regresults = get_reg_results(perioddata, load + '~ MktRF + SMB + HML + RMW + CMA', cov_type)  #
        regresults = regresults.round(2)
        regresults['Portfolio'] = load
        overview_table = overview_table.append(regresults, ignore_index=True)
        del (regresults)

    overview_table_tstats = overview_table[['Portfolio', 't(ave)', 't(a)', 't(MktRF)', 't(SMB)', 't(HML)', 't(RMW)', 't(CMA)', 'R2']]
    overview_table_tstats = overview_table_tstats.rename(
        columns={'t(ave)': 'ave', 't(a)': 'a', 't(MktRF)': 'bMktRF', 't(SMB)': 'bSMB',
                 't(HML)': 'bHML', 't(RMW)': 'bRMW', 't(CMA)': 'bCMA'})

    overview_table_comb = pd.DataFrame(columns=['Portfolio', 'ave', 'a', 'bMktRF', 'bSMB', 'bHML', 'bRMW', 'bCMA', 'R2'])
    for i in range(8):
        overview_table_comb.loc[i * 2 + 1] = \
        overview_table[['ave', 'a', 'bMktRF', 'bSMB', 'bHML', 'bRMW', 'bCMA', 'R2']].applymap(
            lambda x: str(format(x, '.2f'))).loc[i]
        overview_table_comb.loc[i * 2 + 2] = \
        overview_table_tstats[['ave', 'a', 'bMktRF', 'bSMB', 'bHML', 'bRMW', 'bCMA']].applymap(
            lambda x: '(' + str(format(x, '.2f')) + ')').loc[i]
        overview_table_comb.loc[i * 2 + 1, 'Portfolio'] = overview_table.loc[i, 'Portfolio']

    overview_table_comb

    np.log(1 + hport).cumsum().plot()


    # TO-DO
    tab = f.find_between(
        overview_table_comb[['Portfolio', 'ave', 'a', 'bMktRF', 'bSMB', 'bHML', 'bRMW', 'bCMA', 'R2']].fillna('').to_latex(
            index=False), "\\midrule\n", "\\bottomrule")
    tab = tab.replace("Avg.", " \\hline \n Avg.")
    # tab = tab.replace("EW-Comb.1", " \\hline \n EW-Comb.1")

    tab = tab.replace("LbMHb", "$r_{h,MktRF}$")
    tab = tab.replace("LsMHs", "$r_{h,SMB}$")
    tab = tab.replace("LhMHh", "$r_{h,HML}$")
    tab = tab.replace("LrMHr", "$r_{h,RMW}$")
    tab = tab.replace("LcMHc", "$r_{h,CMA}$")
    tab = tab.replace("EW3", " [1ex] \\hline \\\\ [-2ex] \n EW3")
    # tab = tab.replace("EW3", " \\hline \\\\ [-1.5ex] \n $(h_{HML}+h_{RMW}+h_{CMA})/3$")
    # tab = tab.replace("EW4", "$(h_{HML}+h_{RMW}+h_{CMA}+h_{MktRF})/4$")
    # tab = tab.replace("EW5", "$(h_{HML}+h_{RMW}+h_{CMA}+h_{MktRF}+h_{SMB})/5$")


    tab = f.nth_repl(tab, "\\\\\n          &", "\\\\\n \scriptsize{HML,RMW,CMA} &", 6)
    tab = f.nth_repl(tab, "\\\\\n          &", "\\\\\n \scriptsize{EW3+MktRF} &", 6)
    tab = f.nth_repl(tab, "\\\\\n          &", "\\\\\n \scriptsize{EW4+SMB} &", 6)

    tab = """
    \\begin{tabular}{lrrrrrrrr}
    \\hline \\\\ [-2ex]
    Hedge-Portfolio &   Avg. &     $\\alpha$ &  $b_{Mkt-RF}$ &  $b_{SMB}$ &  $b_{HML}$ &  $b_{RMW}$ &  $b_{CMA}$ &    $R^2$ \\\\
    [1ex] \\hline \\\\ [-2ex]
    """ + tab
    tab = tab + "\\hline \n \\end{tabular}"

    text_file = open(tables_path + 'testid' + str(test_id) + '/hedge_regressions.tex', 'w')
    text_file.write(tab)
    text_file.close()
