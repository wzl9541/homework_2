#!/usr/bin/env /shared/share_dff/anaconda3/bin/python
# coding: utf-8

""""
# * Program          : loading_sorted_portfolio_returns.py
# * Function         : calculates loading-sorted portfolios returns.
# * Author           : Kent Daniel, Lira Mota, Simon Rottke and Tano Santos
# * Date             : 2020-01
# * Description      : calculates DMRS loading-sorted portfolios returns.
# * Dependencies     : loading_sorted_portfolios_allocation, stock_monthly
# * Output Tables    : ret
#
# ## Detailed Description:
# Given the DMRS loading-sorted portfolio allocation, calculates portfolio returns.
# All returns saved are EXCESS returns.
#
"""

# %% Packages
import pandas as pd
import numpy as np
import sqlalchemy as sa

import user_setup as uset
import replications.risk_and_returns.loading_sorted_portfolio_allocations as ls_allocation

desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 10)


def main(user):
    print('Started calculating loading-sorted portfolio returns.')
    # %% Parameters
    # User's parameters
    # -----------------
    mysql_vars = uset.user_setup(user)

    # Portfolio Levels
    # -----------------
    # Within each level returns are value weighted.
    port_levels = {'MktRF': [['MEsum_juneportfolio', 'BEMEportfolio', 'bBEMEportfolio'],
                             ['MEsum_juneportfolio', 'OPportfolio', 'bOPportfolio'],
                             ['MEsum_juneportfolio', 'INVportfolio', 'bINVportfolio']],

                   'SMB': [['MEsum_juneportfolio', 'BEMEportfolio', 'sBEMEportfolio'],
                           ['MEsum_juneportfolio', 'OPportfolio', 'sOPportfolio'],
                           ['MEsum_juneportfolio', 'INVportfolio', 'sINVportfolio']],

                    'HML': [['MEsum_juneportfolio', 'BEMEportfolio', 'hBEMEportfolio']],

                    'RMW': [['MEsum_juneportfolio', 'OPportfolio', 'rOPportfolio']],

                    'CMA': [['MEsum_juneportfolio', 'INVportfolio', 'cINVportfolio']]}

    # %% Set MySQL Connection
    conn = sa.create_engine("mysql+pymysql://" + mysql_vars['SQL_USER'] +
                            ":" + mysql_vars['SQL_PASSWORD'] +
                            "@" + mysql_vars['SQL_HOST'] +
                            ":" + mysql_vars['SQL_PORT'] + "/dmrs")

    # %% Download data
    # Run portfolio allocation
    port = ls_allocation.main(user=user)

    # Download returns data (monthly)
    mdata = pd.read_sql_query("""SELECT permno, l.date, rankyear, weighty as weightvar, retadj - rf as exret 
                                 FROM wrds.stock_monthly as l 
                                 INNER JOIN wrds.rf_monthly as r on l.mdate = r.mdate
                                 WHERE rf is not null
                                 AND retadj is not null
                                 AND weighty > 0;""",
                                 conn, 
                              parse_dates=['date'])

    # %% Calculate returns 
    cps = port_levels.keys()
    ret = [None] * len(cps)
    ret = dict(zip(cps, ret))

    for cp in cps:
        temp = pd.merge(mdata, port[cp], on=['rankyear', 'permno'])
        nport_levels = len(port_levels[cp])
        for i in range(nport_levels):
            retcp = temp.groupby(['date']+port_levels[cp][i]).apply(lambda x: np.average(x.exret, weights=x.weightvar)).unstack(level=[1, 2, 3])
            if ret[cp] is None:
                ret[cp] = retcp
            else:
                ret[cp].join(retcp)
        print('Finished calculating loading-sorted portfolio returns for %s.' %cp)

    print('Finished calculating loading-sorted portfolio returns.')
    return ret


if __name__ == "__main__":
    user = 'Lira'
    main(user=user)

