"""
# Set local paths
#
# * Program          : user_setup.py
# * Function         : user_setup
# * Author           : Lira Mota
# * Date             : 2020/01
# * Description      : Set local paths, passwords, etc

# Detailed Description:
#    Any additional user should adapt this code and rename it to "user_setup.py"
"""


def user_setup(username):
    if username == 'Lira':
        mysql = {'SQL_USER': 'xx',
                 'SQL_PASSWORD': 'xx',
                 'SQL_HOST': 'xx',
                 'SQL_PORT': 'xx'
                 }
    else:
        mysql = {'SQL_USER': None,
                 'SQL_PASSWORD': None,
                 'SQL_HOST': None,
                 'SQL_PORT': None
                 }

    return mysql