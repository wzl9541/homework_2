import numpy as np
import pandas as pd
from utils.pk_integrity import *


def post_event_nan(df, event, vars, id_vars=['permno', 'date']):
    """Replace values to na after an event. Data should be correctly sorted.

    Parameters
    ----------
    df: pandas data-frame
        id_vars should not be indexes
    event: boolean series
        event determination
    vars: list
        variables that should be altered.
    id_vars: list
        primary key of data
    """

    event_dt = df[event][id_vars]
    event_dt.rename(columns={id_vars[1]: 'event_date'}, inplace=True)
    pk_integrity(event_dt, id_vars[0])

    df = pd.merge(df, event_dt, on=id_vars[0], how='left')
    df.loc[df[id_vars[1]] > df['event_date'], vars] = np.nan
    df.drop(columns={'event_date'}, inplace=True)

    return df
